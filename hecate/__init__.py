from .manager import *
from . import settings
from . import state
from . import widget

name = "hecate"

__version__ = settings.__version__

__all__ = [
        "settings",
        "manager",
        "state",
        "widget"
          ]

SelectableTextInput --- A fancier text input widget
===================================================

.. autoclass:: hecate.widget.SelectableTextInput
   :members: __init__, entry, select, deselect, boundaries, draw

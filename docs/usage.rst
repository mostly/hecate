Usage
=====

Import the required modules:

.. code:: python

    import curses
    import hecate

Lets define a simple state:

.. code:: python

    class MyState(hecate.state.BaseState):
        def __init__(self, screen, sid):
            hecate.state.BaseState.__init__(self, screen, sid)

        # Override the base function
        def logic(self):
            self.screen_data[0].addstr(1, 1, "Hello World")
            curses.flash()
            self.next_state = "example-sub"

Now lets use this state in a menu:

.. code:: python

    def entry(stdscr):
        # The manager
        h = hecate.Manager(stdscr)
        # The root menu
        state_id = "example-root"
        menu_heading = "Example Menu"
        menu_sub_heading = "A simple menu to learn hecate"
        options = ["Submenu", "Quit"]
        states = ["example-sub", "hecate-quit"]
        h.append_state(hecate.state.StateShiftMenu(h.screen_data, state_id, 
            menu_heading, menu_sub_heading, options, states)
        )
        # The sub menu
        h.append_state(hecate.state.StateShiftMenu(h.screen_data, "example-sub",
            "Submenu", "This is easy", ["My custom state", "Main menu"],
            ["example-custom-state", "example-root"])
        )
        # Our custom state
        h.append_state(MyState(h.screen_data, "example-custom-state"))
        # Start the application loop
        h.application_loop("example-root")

    if __name__=="__main__":
        curses.wrapper(entry)

You can now execute this script with:

.. code:: shell

    python test.py

And you should have the following simple application:

.. figure:: screenshot-usage.png
   :align: center

   Your first custom state ready to go!

.. seealso::
   :class:`BaseState<hecate.state.BaseState>` for a list of methods that can be overidden.
   :class:`Manager<hecate.manager.Manager>` for application loop description of method call order.

.. seealso::
   Check the package examples folder for more fleshed out examples.

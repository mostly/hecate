# import the required modules
import curses
import hecate

# custom state
class MyState(hecate.state.BaseState):
    def __init__(self, screen, sid):
        hecate.state.BaseState.__init__(self, screen, sid)
        # We want to get the user keypresses
        self.events_allowed = True
        # The call list that will be printed
        self.call_list = []
        # For simpler curses calls
        self.screen = self.screen_data[0]

    def set_up(self):
        self.call_list.append("set_up")

    def render(self):
        self.call_list.append("render")
        # A curses clear screen
        self.screen.clear()
        self.screen.refresh()
        curses.doupdate()

    def logic(self):
        self.call_list.append("logic")
        self.screen.addstr(0, 0, str(self.call_list))
        self.call_list = []

    def handle_events(self, event):
        self.call_list.append("handle_events")
        if event in [curses.KEY_ENTER, ord("\n")]:
            self.next_state = "example-submenu"
        else:
            curses.flash()
            curses.beep()


# the wrapped main loop
def entry(stdscr):
    # the manager
    h = hecate.Manager(stdscr)
    # the root menu
    state_id = "example-root"
    menu_heading = "Example Menu"
    menu_sub_heading = "Si non confectus non reficiat"
    options = ["Submenu", "Exit"]
    states = ["example-submenu", "hecate-quit"]
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        state_id, menu_heading, menu_sub_heading, options, states)
    )
    # the sub-menu
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        "example-submenu", "Easy Mode", "idpopqd",
        ["Custom Command", "Return to Main"],
        ["mystate", "example-root"], border=False,
        padding=4)
    ) 
    # the custom state
    h.append_state(MyState(h.screen_data, "mystate"))
    # start the application loop
    h.application_loop("example-root")

# cleanly initializing curses
if __name__=="__main__":
    curses.wrapper(entry)

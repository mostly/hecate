"""hecate - setup.py"""
import setuptools

try:
    import hecate
except ImportError:
    print("error: hecate requires Python 3.5 or greater.")
    quit(1)

LONG_DESC = open('README.md').read()
VERSION = hecate.__version__
DOWNLOAD = "https://gitlab.com/mostly/hecate/archive/%s.tar.gz" % VERSION

setuptools.setup(
    name="hecate",
    version=VERSION,
    author="Shahil Maharaj",
    author_email="maharaj.shahil@gmail.com",
    description="Curses state machine implementation with menus and widgets",
    long_description_content_type="text/markdown",
    long_description=LONG_DESC,
    keywords="curses state machine menu widget",
    license="MIT",
    url="https://gitlab.com/mostly/hecate",
    download_url=DOWNLOAD,
    classifiers=[
        "Environment :: X11 Applications",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    packages=["hecate"],
    python_requires=">=3.5",
    test_suite="tests",
    include_package_data=True,
    zip_safe=False)

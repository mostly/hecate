Canvas --- A widget implementing borders and padding
====================================================

Bases: :class:`hecate.widget.BaseScreen`

.. autoclass:: hecate.widget.Canvas
   :members: __init__, canvas

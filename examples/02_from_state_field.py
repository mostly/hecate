import curses
import hecate

class MyState(hecate.state.BaseState):
    def __init__(self, screen, sid):
        hecate.state.BaseState.__init__(self, screen, sid)
        # We want to get the user keypresses
        self.events_allowed = True
        # For simpler curses calls
        self.screen = self.screen_data[0]

    def logic(self):
        self.screen.addstr(0, 0, "I entered here from <{}>".format(self.from_state))
        self.screen.addstr(1, 0, "Press a key to continue.")

    def handle_events(self, event):
        if event:
            self.next_state = self.from_state


def entry(stdscr):
    h = hecate.Manager(stdscr)
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        "example-root", "From State Example", "Try the Custom State from the Submenu", 
        ["Submenu", "Custom State", "Exit"], 
        ["example-submenu", "mystate", "hecate-quit"])
    )
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        "example-submenu", "Example Submenu", "Try the Custom State from the Main menu",
        ["Custom State", "Return to Main"],
        ["mystate", "example-root"])
    ) 
    # the custom state
    h.append_state(MyState(h.screen_data, "mystate"))
    # start the application loop
    h.application_loop("example-root")

# cleanly initializing curses
if __name__=="__main__":
    curses.wrapper(entry)

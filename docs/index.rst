.. hecate documentation master file, created by
   sphinx-quickstart on Tue Jun  5 08:43:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hecate's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   tips
   hecate

.. image:: ./logo.png

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

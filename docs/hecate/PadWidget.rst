Pad --- A widget implementing scrolling
=======================================

Bases: :class:`hecate.widget.Canvas`

.. autoclass:: hecate.widget.Pad
   :members: __init__, draw

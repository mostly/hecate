Hecate States Reference
=======================
.. toctree::
    :maxdepth: 2

    BaseState
    Quit
    StateShiftMenu
    InputFormState
    ExternalCommandState

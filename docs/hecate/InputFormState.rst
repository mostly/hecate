InputForm --- A form accepting multiple user text inputs
========================================================

Bases: :class:`hecate.state.BaseState`

.. autoclass:: hecate.state.InputForm
   :members: __init__, set_up, tear_down, logic, handle_events, navigate

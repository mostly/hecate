Tips and Tricks
===============

Curses color and Xterm
----------------------

Occasionally the following error may be returned by some terminals:

.. code:: python

    _curses.error: init_pair() returned ERR

When the following line is encountered:

.. code:: python

    curses.init_pair(colour_index+1, colour_index, -1)

In order to rememdy this export the following in your terminal:

.. code:: shell

    export TERM='xterm-256color'

Clean exit from curses
----------------------

It is good practice when using curses to wrap your entry point in order to clean up and restore the terminal settings you started with:

..code:: python

    import curses
    import hecate

    def entry(stdscr):
        # Your code goes in here
        pass

    if __name__="__main__":
        curses.wrapper(entry)

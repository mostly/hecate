import curses
import curses.textpad

from . import widget
from . import utilities

class StateError(Exception):pass
class TextLengthError(StateError):pass
class BadTypeError(StateError):pass
class BadInputError(StateError):pass


class BaseState:
    """A base state to be used in the state machine from which custom states may be derived.
    """
    def __init__(self, screen_data, sid):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **sid** (*str*): Unique string identifying this state.

        Data Members:
          **self.events_allowed** (*bool*): Flag determining program flow in the *get_events* member function

          **self.next_state** (*str*): When this value is not **None** and contains the sid of a :class:`BaseState<hecate.state.BaseState>` stored in the *state-dictionary* then the manager will change to that state in the next application loop. This value is therefore typically modified in the *logic* or *handle_events* member functions.

          **self.kill_hecate** (*bool*): This this value is **True** the manager will terminate during the next application loop. By default the manager will already contain a :class:`Quit<hecate.state.Quit>` state that will perform this task by setting *self.next_state* to **hecate-quit** but this member exists to allow the user to generate their own class should further logic be required.

          **self.from_state** (*str*): This value is set by the manager *_set_state* member function and tells this object from which state-object the manager entered.

          **self.message** (*ANY*): This value is passed from one state to the state that follows it.

        .. seealso::

           :class:`load_screen_data<utilities.load_screen_data>`
        """
        self.sid = sid
        self.events_allowed = False
        sd = utilities.load_screen_data(screen_data)
        self.screen_data = sd
        self.next_state = None
        self.kill_hecate = False
        self.from_state = None
        self.message = None

    def set_up(self):
        """Base set up function.

        This function is called whenever a state is set as the current state in the manager.
        """
        pass

    def clear(self):
        """Base clear function.

        This function is not called by the manager but by :class:`render<hecate.state.BaseState.render>`. It exists so the user may modify it in a derived class to edit other window behavior while using the default render method.
        """
        self.screen_data[0].clear()
        self.screen_data[0].refresh()
        curses.doupdate()

    def render(self):
        """Base render function.

        This function is called as Step 1 for each iteration of :class:`application_loop<hecate.manager.Manager.application_loop>`.
        """
        self.clear()

    def logic(self):
        """Base logic loop.

        This function is called as Step 3 for each iteration of :class:`application_loop<hecate.manager.Manager.application_loop>`.
        """
        pass

    def get_events(self):
        """Base event gathering function.

        This function is called as Step 4 for each iteration of :class:`application_loop<hecate.manager.Manager.application_loop>`.
        """
        if self.events_allowed:
            self.handle_events(self.screen_data[0].getch())

    def handle_events(self, event):
        """Base event handling function.

        This function is not called by the manager but by the :class:`get_events<hecate.state.BaseState.get_events>`. It exists so that the user may modify it in a derived class to edit event handling behaviour while using the default get_events method.
        """
        pass

    def tear_down(self):
        """Base tear down function.

        This function is called whenever the manager changes from this state to another state.
        """
        pass


class Quit(BaseState):
    """A quitting state that is automatically added to :class:`Manager<hecate.manager.Manager>` during initialization.

    This state sets the *self.kill_hecate* flag to *True* which causes the manager application loop to terminate on the next iteration.
    """
    def __init__(self, screen_data):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

        Data Members:
           **self.sid** (*str*): Set to *hecate-quit*.

           **self.kill_hecate** (*bool*): Set to *True*.
        """
        sid = "hecate-quit"
        BaseState.__init__(self, screen_data, sid)
        self.kill_hecate = True


class StateShiftMenu(BaseState):
    """A menu state with built-in scrolling and minimal aesthetic options.
    """
    def __init__(self, screen_data, sid, heading, sub_heading, options, 
            states, border=True, padding=1, escape_state=None, parent_state=None):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **sid** (*str*): Unique string identifying this state.

           **heading** (*str*): Menu heading.

           **sub_heading** (*str*): Menu sub-heading.

           **options** (*list(str)*): List of menu options as strings.

           **states** (*list(str)*): List of the state sid that activation of an option should cause traversal to.

        Kwargs:
           **border** (*bool*): Should a border be drawn around the menu?

           **padding** (*int*): Padding value for spacing menu content. This value should be in the range [0, 9].

           **escape_state** (*str*): sid of state to return to in case ESCAPE key is pressed.

           **parent_state** (*str*): sid of parent state, used during set_up to determin cursor position.

        Data Members:
           **self.position** (*int*): Menu item that is highlighted and that is activated if **Enter** is pressed. This value can take on values that are valid indeces for self.options. This value is modified internally.

           **self.menu_text** (*list(str)*): This is a list containing the stylistic layout for this menu implementation. It is used during pad position calculations that are sent to :class:`Pad<hecate.widget.Pad>`.

           **self.pad** (:class:`Pad<hecate.widget.Pad>`): A pad widget used to implement menu scrolling.

        .. note::
           
           The length of self.heading, self.sub_heading are determined by the application screen data in the following manner:
           **max_length** = screen_width - (2 x padding_value) - (2 x padding_present)

        .. note::

           Each option value should not include the menu numbering or spacing. Additionally it should be noted that the maximum length of each option value is **5 less** than the max_length for the heading due to the numbering characters suffixed to the option during rendering.

        .. warning::

           **self.options** and **self.states** exists on a *one-to-one* relationship and should be of the same length.

        .. seealso::

           :class:`load_screen_data<utilities.load_screen_data>`
        """
        BaseState.__init__(self, screen_data, sid)
        self.events_allowed = True
        self.position = 0
        self.options = options
        self.states = states
        self._validate_lists()
        self.menu_text = [heading, "space", sub_heading, "space"]
        self.pad = widget.Pad(self.screen_data, 
                len(self.menu_text)+len(options), border, padding)
        self._validate()
        self.escape_state = escape_state
        self.parent_state = parent_state

    def _validate_lists(self):
        """This function is used internally to check initialization arguments.

        Raises:
           BadTypeError, BadInputError

        This function checks the following:

        * self.options is a list.
        * self.states is a list.
        * self.options and self.states are of the same length.
        * self.options is not empty.
        """
        if type(self.options) != list:
            d = ("Error: Encountered " + str(type(self.options)) 
                    + " for option argument.\nList expected.")
            raise BadTypeError(d)
        if type(self.states) != list:
            d = ("Error: Encountered " + str(type(self.options)) 
                    + " for option argument.\nList expected.")
            raise BadTypeError(d)
        if len(self.options) != len(self.states):
            d = ("Error: Mismatching lengths options<" 
                    + str(len(self.options)) + "> states<" 
                    + str(len(self.states)) + ">.")
            raise BadInputError(d)
        if len(self.options) == 0:
            d = ("Error: Empty options argument. Non-empty argument " 
                    + "expected.")
            raise BadInputError(d)

    def _validate(self):
        """This function is used internally to check initialization arguments.

        Raises:
           TextLengthError

        This function checks the following:

        * self.heading is of a valid length.
        * self.sub_heading is of a valid length.
        * self.options contains entries of a valid length.
        """
        max_width = self.pad.max_width
        if len(self.menu_text[0]) > max_width:
            d = ("Error: StateShiftMenu heading <" 
                    + str(len(self.menu_text[0])) + "> exceeds current " 
                    + "maximum heading length <" + str(max_width) 
                    + ">.")
            raise TextLengthError(d)
        if len(self.menu_text[2]) > max_width:
            d = ("Error: StateShiftMenu sub_heading <" 
                    + str(len(self.menu_text[2])) + "> exceeds current " 
                    + "maximum sub-heading length <" + str(max_width) 
                    + ">.")
            raise TextLengthError(d)
        # Options have additional space for "num - " preceding option
        max_width -= 5
        for index, option in enumerate(self.options):
            if len(option) > max_width:
                d = ("Error: StateShiftMenu initialized with option[" 
                        + str(index) + "] which has length <" 
                        + str(len(option)) + "> that exceeds current " 
                        + "maximum option length <" + str(max_width) 
                        + ">.")
                raise TextLengthError(d)

    def set_up(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        If the manager entered this state from a non-child state then the *self.position* is set to 0.
        """
        curses.curs_set(0)
        self.pad.local.clear()
        if self.parent_state:
            if self.from_state == self.parent_state:
                self.position = 0

    def logic(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        This is the function that draws the menu.
        """
        y = 0
        self.pad.canvas().addstr(y, 0, self.menu_text[0], 
                curses.A_BOLD | curses.A_UNDERLINE)
        y += 2
        self.pad.canvas().addstr(y, 0, self.menu_text[2],
                curses.A_UNDERLINE)
        y += 2
        for index, value in enumerate(self.options):
            if index == self.position:
                mode = curses.A_REVERSE
            else:
                mode = curses.A_NORMAL
            message = "{} - {}".format(index+1, value)
            self.pad.canvas().addstr(y, 0, message, mode)
            y += 1
        self.pad.draw(self.position+len(self.menu_text))

    def handle_events(self, event):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        Events handled:
        
        +-----------+------------------------------------+
        |   Event   |              Action                |
        +===========+====================================+
        | KEY_ENTER | Set next state to states[position] |
        +-----------+------------------------------------+
        |   KEY_UP  | Move up menu                       |
        +-----------+------------------------------------+
        |  KEY_DOWN | Move down menu                     |
        +-----------+------------------------------------+
        """
        if event in [curses.KEY_ENTER, ord("\n")]:
            self.next_state = self.states[self.position]
        elif event == curses.KEY_UP:
            self.navigate(-1)
        elif event == curses.KEY_DOWN:
            self.navigate(1)
        elif event == 27:
            if self.escape_state:
                self.next_state = self.escape_state

    def navigate(self, delta):
        """This function determines which element is selected in the menu.
        """
        self.position += delta
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.options):
            self.position = len(self.options) - 1


class InputForm(BaseState):
    """A state that implements a form taking user data via :class:`SelectableTextWidgets<hecate.widget.SelectableTextWidget>`.
    """
    def __init__(self, screen_data, sid, heading, sub_heading, entry_lengths, 
            labels, messages, allowed, orientation, quit_state, save_state, 
            quit_key=27, save_key=ord(" "), border=True, padding=1):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **sid** (*str*): Unique string identifying this state.

           **heading** (*str*): Form heading.

           **sub_heading** (*str*): Form sub-heading.

           **entry_lengths** (*list[int]*): The column width of each entry field.

           **labels** (*list[str]*): The labels for each entry field.

           **messages** (*list[str]*): The initial state for each entry field.

           **orientation** (*int*): *widget.VERTICAL* or *widget.HORIZONTAL*

           **allowed** (*list[str]*): The allowed-character filter for each entry field.

           **quit_state** (*str*): sid of the state to change to when a quit event is detected.

           **save_state** (*str*): sid of the state to change to when a save event is detected.

        Kwargs:
           **quit_key** (*int*): Curses event constant to be used for quitting the form without saving.

           **save_key** (*int*): Curses event constant to be used for saving and quitting the form.

           **border** (*bool*): Should a border be drawn around the menu?

           **padding** (*int*): Padding value for spacing menu content. This value should be in the range [0, 9].

        Data Members:
           **self.row** (*int*): The currently selected row position. Used internally during rendering.

           **self.col** (*int*): The currently selected col position. Used internally during rendering.

           **self.form_text** (*list(str)*): This is a list containing the stylistic layout for this form implementation. It is used during pad position calculations that are sent to :class:`Pad<hecate.widget.Pad>`.

           **self.pad** (:class:`Pad<hecate.widget.Pad>`): A pad widget used to implement scrolling.
 
           **self.entries** (list(:class:`SelectableTextInput<hecate.widget.SelectableTextInput>`)): A list containing the text input widgets.

           **self.results** (*list(str)*): A list containing the user entered values from the text input widgets.
        """
        BaseState.__init__(self, screen_data, sid)
        self.events_allowed = True
        self.row = 0
        self.col = 0
        self.form_text = [heading, "space", sub_heading, "space"]
        pad = widget.Pad(self.screen_data, 
                len(self.form_text), border, padding)
        e = 0
        total_rows = 0
        col_pos = 1
        end_pos = 0
        pad_width = pad.canvas().getmaxyx()[1]
        self.widget_width = []
        widget_width = 0
        unfilled_row = False
        while e < len(entry_lengths):
            if orientation == widget.VERTICAL:
                widget_width = max([len(labels[e]), entry_lengths[e]]) + 2
            else:
                widget_width = len(labels[e]) + entry_lengths[e] + 3
            end_pos = col_pos + widget_width
            if end_pos >= pad_width:
                if col_pos == 1:
                    d = ("Error: Unable to draw form because the " 
                            + "maximum widget length for form entries " 
                            + "provided <" + str(widget_width) + "> which " 
                            + "exceeds window width <" 
                            + str(pad_width) + ">. Error was encountered "
                            + "on entry <" + str(e) + ">.")
                    raise BadInputError(d)
                else:
                    col_pos = 1
                    total_rows += 1
                    unfilled_row = False
            else:
                col_pos = end_pos
                self.widget_width.append(widget_width)
                e += 1
                unfilled_row = True
        if unfilled_row:
            total_rows += 1
        widget_height = 4
        self.pad = widget.Pad(self.screen_data,
                len(self.form_text)+(total_rows*widget_height)-1, 
                border, padding)
        self.entries = []
        self.results = []
        self.entry_lengths = entry_lengths
        self.labels = labels
        self.messages = messages
        self.allowed = allowed
        self.quit_state = quit_state
        self.save_state = save_state
        self.orientation = orientation
        self.widget_height = widget_height
        self.quit_key = quit_key
        self.save_key = save_key

    def set_up(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        Creates the text widgets and sets the selected position to 0. This allows the widgets to be cleared by *tear_down*.
        """
        curses.curs_set(0)
        self.pad.local.clear()
        self.message = None
        self.row = 0
        self.col = 0
        row_pos = 0
        col_pos = 1
        row_buffer = []
        msg_buffer = []
        e = 0
        pad_width = self.pad.canvas().getmaxyx()[1]
        while e < len(self.entry_lengths):
            end_pos = col_pos + self.widget_width[e]
            if end_pos >= pad_width:
                col_pos = 1
                row_pos += 1
                self.entries.append(row_buffer)
                self.results.append(msg_buffer)
                row_buffer = []
                msg_buffer = []
            else:
                row_buffer.append(widget.SelectableTextInput(
                    self.pad, self.entry_lengths[e], 
                    len(self.form_text)+(row_pos*self.widget_height)+1,
                    col_pos, self.labels[e], self.orientation,
                    self.messages[e], self.allowed[e]))
                msg_buffer.append(self.messages[e])
                col_pos = end_pos
                e += 1
        if len(row_buffer) > 0:
            self.entries.append(row_buffer)
            self.results.append(msg_buffer)
        self.entries[self.row][self.col].select()

    def tear_down(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`

        The text input widgets are cleared so that subsequent calls to the state use are cleared upon exit.
        """
        self.entries = []
        self.results = []

    def logic(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        This is the function that draws the form.
        """
        if self.row == len(self.entries) - 1:
            self.pad.draw((self.row*self.widget_height) 
                    + (self.widget_height-1) + len(self.form_text))
        self.pad.canvas().clear()
        y = 0
        self.pad.canvas().addstr(y, 0, self.form_text[0], 
                curses.A_BOLD | curses.A_UNDERLINE)
        y += 2
        self.pad.canvas().addstr(y, 0, self.form_text[2],
                curses.A_UNDERLINE)
        y += 2
        for row in range(len(self.entries)):
            for col in range(len(self.entries[row])):
                self.entries[row][col].draw()
        self.pad.draw((self.row*self.widget_height) 
                + (self.widget_height-1) + len(self.form_text))


    def handle_events(self, event):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        Events handled:
        
        +-----------+------------------------------------------------+
        |   Event   |                     Action                     |
        +===========+================================================+
        | KEY_ENTER | Text entry on self.entries[self.row][self.col] |
        +-----------+------------------------------------------------+
        |   KEY_UP  | Move selection rectangle up                    |
        +-----------+------------------------------------------------+
        |  KEY_DOWN | Move selection rectangle down                  |
        +-----------+------------------------------------------------+
        |  KEY_LEFT | Move selection rectangle left                  |
        +-----------+------------------------------------------------+
        | KEY_RIGHT | Move selection rectangle right                 |
        +-----------+------------------------------------------------+
        |   ESCAPE  | Exit without saving                            |
        +-----------+------------------------------------------------+
        |  SPACEBAR | Save and exit                                  |
        +-----------+------------------------------------------------+
        """
        if event in [curses.KEY_ENTER, ord("\n")]:
            self.results[self.row][self.col] = (
                    self.entries[self.row][self.col].entry()
                    )
        elif event == curses.KEY_UP:
            self.navigate(-1, 0)
        elif event == curses.KEY_DOWN:
            self.navigate(1, 0)
        elif event == curses.KEY_LEFT:
            self.navigate(0, -1)
        elif event == curses.KEY_RIGHT:
            self.navigate(0, 1)
        elif event == self.quit_key:
            self.next_state = self.quit_state
        elif event == self.save_key:
            self.message = self.results
            self.next_state = self.save_state

    def navigate(self, row_delta, col_delta):
        """This function determines which element is selected in the menu.
        """
        row = self.row
        col = self.col
        self.entries[self.row][self.col].deselect()
        row += row_delta
        col += col_delta
        if row < 0:
            row = 0
        elif row >= len(self.entries):
            row = len(self.entries) - 1
        if col < 0:
            col = 0
        elif col >= len(self.entries[row]):
            col = len(self.entries[row]) - 1
        self.row = row
        self.col = col
        self.entries[self.row][self.col].select()


class ExternalCommand(BaseState):
    """A state that saves the curses state so that an external terminal program may be called.
    """
    def __init__(self, screen_data, sid):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **self.sid** (*str*): Unique string ideintifying this state.

        """
        BaseState.__init__(self, screen_data, sid)

    def set_up(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        Saves the curses state.
        """
        curses.def_prog_mode()
        self.screen_data[0].clear()

    def tear_down(self):
        """This function overrides the function from the base class :class:`BaseState<hecate.state.BaseState>`.

        Restores the curses state.
        """
        curses.reset_prog_mode()
        curses.curs_set(1)
        curses.curs_set(0)
        self.screen_data[0].clear()

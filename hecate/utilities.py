import curses

from . import settings

class UtilitiesError(Exception):pass
class BadScreenDataError(UtilitiesError):pass

VALID_KEYS = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
              "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", 
              "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", 
              "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
              "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", 
              "9", "0", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", 
              "_", "+", "-", "=", "[", "]", "\\", "{", "}", "|", ";", ":",
              '"', "'", ",", ".", "/", "<", ">", "?", " ", "`", "~"]

def load_screen_data(screen_data):
    """This function loads different variants of the screen_data argument supported by Hecate.

    Args:
       **screen_data** : This argument can be supplied in two ways:

       #. As a _curses.window returned from *curses.initscr()* or ideally from *curses.wrapper()*
       #. As a list in the following format: [_curses.window int] where:

              * the first element is a _curses.window object as in point 1 above
              * the second element is a positive integer representing the minimum screen width that can be handled by your application
           
       If the first method is employed a *minimum screen width* of **80** is used.

    Raises: 
       BadScreenDataError

    Returns:
       [_curses.window minimum_screen_width]
    """
    if type(screen_data) == list:
        if len(screen_data) < 2:
            d = ("Error: length of list provided to Hecate <" 
                    + str(len(screen_data)) + ">.\nExpected length <2>")
            raise BadScreenDataError(d)
        if str(type(screen_data[0])) != "<class '_curses.window'>":
            d = ("Error: Unhandled data-type encountered for " 
                    + "screen_data[0] <" + str(type(screen_data[0])) 
                    + ">.\n Expected <class '_curses.window'>.")
            raise BadScreenDataError(d)
        if type(screen_data[1]) != int:
            d = ("Error: Unhandled data-type encountered for " 
                    + "screen_data[1] <" + str(type(screen_data[1])) 
                    + ">.\n Expected <class 'int'>.")
            raise BadScreenDataError(d)
        return screen_data
    elif str(type(screen_data)) == "<class '_curses.window'>":
        return [screen_data, settings.MIN_SCREEN_WIDTH]
    else:
        d = ("Error: Unhandled data-type encountered for screen_data <" 
                + str(type(screen_data)) + ">.\nExpected <class " + 
                "'_curses.window'> OR <class 'list(_curses_window, " 
                + "int)'>")
        raise BadScreenDataError(d)


def interpret_text_entry(entry):
    """This function returns a character for an input key.

    Args:
       **entry** (*int*): An input key taken from *curses.window.getch()*.

    Returns:
       (*str*) : The character value of the key if it is a valid text entry key.
       (*None*) : If the key is invalid.
    """

    if chr(entry) in VALID_KEYS:
        return chr(entry)
    else:
        return None

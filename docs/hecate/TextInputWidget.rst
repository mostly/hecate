TextInput --- The base widget implementing a text box entry field
=================================================================

.. autoclass:: hecate.widget.TextInput
   :members: __init__, _build_render, add_character, remove_character, entry, draw, activate

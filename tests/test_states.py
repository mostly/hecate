"""Test hecate states classes and helper functions"""
import unittest
import curses

from hecate import state
from hecate import settings
from hecate import utilities

class TestStates(unittest.TestCase):
    def setUp(self):
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)

    def tearDown(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    def test_hecatestate_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            state.BaseState(0, "test")

    def test_hecatestate_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            state.BaseState([self.stdscr], "test")

    def test_hecatestate_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            state.BaseState([0, 0], "test")

    def test_hecatestate_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            state.BaseState([self.stdscr, "hello"], "test")

    def test_hecatestate_init_window_width_default(self):
        h = state.BaseState(self.stdscr, "test")
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_hecatestate_init_window_width_custom(self):
        h = state.BaseState([self.stdscr, 60], "test")
        self.assertEquals(h.screen_data[1], 60)

    def test_hecatequit_init(self):
        h = state.Quit(self.stdscr)
        self.assertEquals(h.sid, "hecate-quit")
        self.assertTrue(h.kill_hecate)

    def test_hecatestateshiftmenu_init_long_heading(self):
        heading = ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        with self.assertRaises(state.TextLengthError):
            state.StateShiftMenu(self.stdscr, "test", heading, 
                    "sub_heading", [1], [1])

    def test_hecatestateshiftmenu_init_long_subheading(self):
        sub_heading = ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        with self.assertRaises(state.TextLengthError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    sub_heading, [1], [1])

    def test_hecatestateshiftmenu_init_non_list_options(self):
        with self.assertRaises(state.BadTypeError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", 5, [1])

    def test_hecatestateshiftmenu_init_non_list_states(self):
        with self.assertRaises(state.BadTypeError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", [5], 1)

    def test_hecatestateshiftmenu_init_empty_options(self):
        with self.assertRaises(state.BadInputError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", [], [1])

    def test_hecatestateshiftmenu_init_empty_states(self):
        with self.assertRaises(state.BadInputError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", [1], [])

    def test_hecatestateshiftmenu_init_mismatched_options_states(self):
        with self.assertRaises(state.BadInputError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", [5], [1, 2])

    def test_hecatestateshiftmenu_init_long_option(self):
        long_option = ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" 
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        with self.assertRaises(state.TextLengthError):
            state.StateShiftMenu(self.stdscr, "test", "heading", 
                    "sub_heading", [long_option], [1])

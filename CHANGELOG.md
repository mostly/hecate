# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]
- InputForm tests
- Utility tests

## [1.2.2] - 2018-06-28

### Fixed

- *VALID_KEYS* list in utilities used by text input widgets to interpret keys was missing a comma and thus not accepting certain inputs.

## [1.2.1] - 2018-06-13

### Added

- *self.message* member to *Manager* class that is passed from the outgoing state to the incoming state and is a method by which data can be shared between states. See example 04.

### Removed

- *read\_form\_data* and *save\_form\_data* functions from utilities as well as their dependancies. This was a bad solution.

## [1.2.0] - 2018-06-12

### Added

- *read\_form\_data* and *save\_form\_data* functions to utilities for use with accessing *InputForm* data.
- *ExternalCommand* a state implementing a call to an external terminal program. This state preserves the curses state at *set\_up* and restores it at *tear\_down*

### Changed

- Updated example-04 to access form data within a save state.

## [1.1.2] - 2018-06-12

### Changed

- Packing method used by *InputForm* now packs as many widgets as it can in a row before moving to the next row.
- *example* folder to *examples*

### Added

- **BREAKING CHANGE**: Added orientation argument to *InputForm* allowing **VERTICAL** or **HORIZONTAL** widget orientation. Defaults to **VERTICAL**.
- *quit\_key* and *save\_key* kwargs to *InputForm* to implement custom save and quit key events.
- *escape\_state* kwarg to *StateShiftMenu* that represents the state that will be traversed to if the *ESCAPE* key is pressed. Defaults to None meaning nothing happens when the key is pressed without this set up.
- *parent\_state* kwarg to *StateShiftMenu* that stores the parent state for the menu and is used by *set\_up* to determine cursor position.
- Text input example
- Form input example

## [1.1.1] - 2018-06-12

### Changed

- **BREAKING CHANGE**: Bug in *TextInput* widget that did not render input to screen until return pressed when parent was a *widget.Pad*. *TextInput* now takes a *HecateCanvas* derived object as *parent* argument rather than a *curses window*.

### Added

- *InputForm* state : a state implementing a form in which the user may input data for a number of fields.

## [1.1.0] - 2018-06-08

### Fixed

- Erroneous date in Changelog 1.0.1

### Changed

- **BREAKING CHANGE**: TextInput initialization now passed window dimensions and the derived window is created internally.

### Added

- *SelectableTextInput* : a widget that draws a border around itself to indicate selection. This widget also implements a simple label for the text field.
- *activation()* method added to *TextInput* widget

## [1.0.1] - 2018-06-07

### Fixed

- Erroneous *GitHub* references in setup.py.
- Outdated Sphinx build command in README.md describing document build.
- Removed widget validate functions which were not working correctly and placed the checking code directly in each relevant init to ensure it is run.
- widget.Window.draw bug that performed a pad refresh rather than a window refresh.
- *StateShiftMenu* now clears self.pad.local instead of self.screen\_data[0].

### Added

- *TextInput* widget
- utilities.interpret\_text\_event used by TextInput
- *TextInput* tests
- Extra test for colour mode in manager
- Documentation tips and tricks

### Changed

- *BaseWidget* to *BaseScreen*

## [1.0.0] - 2018-06-06

### Added

- *Manager* class : this manages the state-machine.
- *BaseState* class : all states derived from this.
- *Quit* class class : the quitting state.
- *StateShiftMenu* class : a state implementing a scrolling menu.
- *BaseWidget* class : all widgets are derived from this.
- *Canvas* class : implements borders and padding.
- *Pad* class : implements a scrolling *Canvas*.
- *Window* class : implements a fixed *Canvas*.
- Descriptive README.md.
- Sphinx documentation.
- Unit-tests for manager module.
- Unit-tests for state module.
- Unit-tests for widget module.
- Basic example.
- *from\_state* field example.
- Set up installation via pip.
- Changelog.

[Unreleased]: https//gitlab.com/mostly/hecate/compare/1.2.2...HEAD
[1.2.2]: https://gitlab.com/mostly/hecate/compare/1.2.1...1.2.2
[1.2.1]: https://gitlab.com/mostly/hecate/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/mostly/hecate/compare/1.1.2...1.2.0
[1.1.2]: https://gitlab.com/mostly/hecate/compare/1.1.1...1.1.2
[1.1.1]: https://gitlab.com/mostly/hecate/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/mostly/hecate/compare/1.0.1...1.1.0
[1.0.1]: https://gitlab.com/mostly/hecate/compare/1.0.0...1.0.1

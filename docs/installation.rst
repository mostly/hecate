Installation
============

This packages is currently only available through gitlab. Get the package data into your home folder:

#. For git users:

.. code:: shell

    git clone https://gitlab.com/mostly/hecate.git

#. For non-git users, download a zip and extract it.

This package has been prepared for installation via pip. The top-level folder containing the *README.md* file will be referred to as the **root-folder**. Change to the **root folder** and run:

.. code:: shell

    pip install --upgrade .

For a system-wide installation. If you wish for a local-user installation type:

.. code:: shell

    pip install --user --upgrade .

Requirements:

* curses (*Python 3.6.5*)

.. warning::
   This package should be cross-platform compatible but has only been tested on Linux.

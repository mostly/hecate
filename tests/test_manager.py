"""Test hecate class and helper functions"""
import unittest
import curses

from hecate import manager
from hecate import state
from hecate import settings
from hecate import utilities

class TestState(state.BaseState):
    def __init__(self, screen_data, sid):
        state.BaseState.__init__(self, screen_data, sid)
        self.call_list = []
        self.call_list.append("init")

    def set_up(self):
        self.call_list.append("set_up")

    def tear_down(self):
        self.call_list.append("tear_down")

    def clear(self):
        self.call_list.append("clear")

    def render(self):
        self.call_list.append("render")
        self.clear()

    def get_events(self):
        self.call_list.append("get_events")
        self.handle_events()
        self.next_state = "hecate-quit"

    def logic(self):
        self.call_list.append("logic")

    def handle_events(self):
        self.call_list.append("handle_events")

class TestHecate(unittest.TestCase):
    def setUp(self):
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)

    def tearDown(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    def test_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            manager.Manager(0)

    def test_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            manager.Manager([self.stdscr])

    def test_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            manager.Manager([0, 0])

    def test_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            manager.Manager([self.stdscr, "hello"])

    def test_init_window_width_default(self):
        h = manager.Manager(self.stdscr)
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_init_window_width_custom(self):
        h = manager.Manager([self.stdscr, 60])
        self.assertEquals(h.screen_data[1], 60)

    def test_append_state_duplicate(self):
        h = manager.Manager(self.stdscr)
        s_1 = TestState(self.stdscr, "test1")
        h.append_state(s_1)
        s_2 = TestState(self.stdscr, "test1")
        with self.assertRaises(manager.DuplicateSidError):
            h.append_state(s_2)

    def test_append_state_larger_screen_width(self):
        h = manager.Manager(self.stdscr)
        s = TestState([self.stdscr, 100], "test")
        with self.assertRaises(manager.ScreenDimensionsError):
            h.append_state(s)

    def test_append_state_successful(self):
        h = manager.Manager(self.stdscr)
        s = TestState(self.stdscr, "test")
        self.assertEquals(len(h.states), 1)
        h.append_state(s)
        self.assertEquals(len(h.states), 2)

    def test_set_state_bad_sid(self):
        h = manager.Manager(self.stdscr)
        s = TestState(self.stdscr, "test")
        h.append_state(s)
        with self.assertRaises(manager.BadSidError):
            h._set_state("toast")

    def test_set_state_successful(self):
        h = manager.Manager(self.stdscr)
        s = TestState(self.stdscr, "test")
        h.append_state(s)
        h._set_state("test")
        self.assertEqual(h.states["test"].from_state, None)
        self.assertEqual(h.states["test"].call_list, 
                ["init", "set_up"])

    def test_change_state_no_current_state(self):
        h = manager.Manager(self.stdscr)
        s = TestState(self.stdscr, "test")
        h.append_state(s)
        with self.assertRaises(manager.BadStateError):
            h._change_state()

    def test_change_state_successful(self):
        h = manager.Manager(self.stdscr)
        s_1 = TestState(self.stdscr, "test1")
        s_2 = TestState(self.stdscr, "test2")
        h.append_state(s_1)
        h.append_state(s_2)
        h._set_state("test1")
        h.current_state.next_state = "test2"
        h._change_state()
        self.assertEquals(["init", "set_up", "tear_down"], 
                h.states["test1"].call_list)
        self.assertEquals(None, h.states["test1"].next_state)
        self.assertEquals(["init", "set_up"], 
                h.states["test2"].call_list)
        self.assertEquals("test1", h.states["test2"].from_state)
        self.assertEquals("test2", h.current_state.sid)

    def test_application_loop(self):
        h = manager.Manager(self.stdscr)
        s = TestState(self.stdscr, "test")
        h.append_state(s)
        h.application_loop("test")
        self.assertEquals(h.states["test"].call_list, 
                ["init", "set_up", "render", "clear", "logic", 
                    "get_events", "handle_events", "render", "clear", 
                    "tear_down"]
        )

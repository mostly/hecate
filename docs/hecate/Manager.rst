Manager --- State-machine application class
===========================================

.. autoclass:: hecate.manager.Manager
    :members: __init__, append_state, application_loop, _change_state, _set_state

BaseState --- The base state managed by Hecate
==============================================

.. autoclass:: hecate.state.BaseState
   :members: __init__, set_up, clear, render, logic, get_events, handle_events, tear_down

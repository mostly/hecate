StateShiftMenu --- A scrolling menu state
=========================================

Bases: :class:`hecate.state.BaseState`

.. autoclass:: hecate.state.StateShiftMenu
   :members: __init__, _validate_lists, _validate, set_up, logic, handle_events, navigate

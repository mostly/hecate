Window --- A widget implementing a fixed window
===============================================

Bases: :class:`hecate.widget.Canvas`

.. autoclass:: hecate.widget.Window
   :members: __init__, draw

import curses

from . import utilities

class WidgetError(Exception):pass
class DimensionsError(WidgetError):pass
class BadInputError(WidgetError):pass

VERTICAL = 0
HORIZONTAL = 1

class BaseScreen:
    """A base widget encapsulating screen data.
    """
    def __init__(self, screen_data):
        """Widget constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

        Raises:
           DimensionsError

        .. seealso::

           :class:`load_screen_data<utilities.load_screen_data>`
        """
        self.screen_data = utilities.load_screen_data(screen_data)
        if self.screen_data[1] < 1:
            d = ("Error: Unable to initialize HecateWidget with width " 
                    + "smaller than <1>.")
            raise DimensionsError(d)
        if self.screen_data[0].getmaxyx()[1] < self.screen_data[1]:
            d = ("Error: Unable to initialize HecateWidget in a screen " 
                    + "with width smaller than <" 
                    + str(self.screen_data[1]) 
                    + ">. Please resize your terminal and try again.")
            raise DimensionsError(d)


class Canvas(BaseScreen):
    """A derived class that implements borders, and padding for a list of input rows.
    """
    def __init__(self, screen_data, rows, border, padding):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **rows** (*int*): The number of rows to be rendered by the canvas.

           **border** (*bool*): Should a border be rendered?

           **padding** (*int*): Padding space around the boundaries of the canvas.

        Raises:
           BadInputError

        Data Members:
           **self.derived_rows** (*int*): Height of the derived window.

           **self.local_rows** (*int*): Height of the local window.

           **self.padwad** (*int*): Variable used to calculated spacing if padding is requested.

           **self.max_width** (*int*): The maximum length of any rows to be rendered taking into account padding and borders.

           **self.local** (*curses.window*): The base curses window to which the canvas is rendered.

           **self.derived** (*curses.window*): The window derived from the base window should borders be required. This simplifies co-ordinates.
        """
        BaseScreen.__init__(self, screen_data)
        self.border = border
        self.padding = padding
        if self.padding < 0 or self.padding > 9:
            d = ("Error: Canvas attribute self.padding <"
                    + str(self.padding) + ">does not lie within range " 
                    + "[0, 9].")
            raise BadInputError(d)
        self.derived_rows = rows
        if rows < 1:
            d = ("Error: Cannot create Canvas with 0 height.")
            raise BadInputError(d)
        self.local_rows = self.derived_rows
        self.padwad = 0
        if self.padding > 0:
            self.local_rows += (2 * self.padding)
            self.padwad = 1
        self.max_width = (self.screen_data[1] - (2 * self.padding) - 
                (2 * self.padwad))
        self.local = None
        self.derived = None

    def canvas(self):
        """This access function returns the derived window.
        """
        return self.derived


class Pad(Canvas):
    """A derived class implementing window scrolling.
    """
    def __init__(self, screen_data, rows, border, padding):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **rows** (*int*): The number of rows to be rendered by the canvas.

           **border** (*bool*): Should a border be rendered?

           **padding** (*int*): Padding space around the boundaries of the canvas.

        Data Members:
           **self.scroll_position** (*int*): Position tracker used to render relevant window portions to the screen.

           **self.local** (*curses.window*): Implemented as a curses pad with dimensions large enough to hold all rows.

           **self.derived** (*curses.window*): Implemented as a curses derived window with the dimensions provided by the screen.

           **self.last_drawn_position** (*int*): Tracks last drawn position, used by :class:`TextInput<hecate.widget.TextInput>` for *draw* updates.
        """
        Canvas.__init__(self, screen_data, rows, border, padding)
        self.scroll_position = 0
        self.local = curses.newpad(self.local_rows, self.screen_data[1])
        self.derived = self.local.derwin(self.derived_rows, self.max_width, 
                self.padding, self.padding+self.padwad)
        self.last_drawn_position = 0

    def draw(self, position=None):
        """This function renders the relevant portion of the local window to the screen.

        Args:
           **position** (*int*): The row number on the screen to render from.

        .. versionadded:: 1.1.1

           The newly-added data member self.last_drawn_position tracks the last used position value in order to allow external classes like :class:`TextInput<hecate.widget.TextInput>` to make draw calls to a Pad to it last used position.
        """
        if position == None:
            position = self.last_drawn_position
        if self.border:
            self.local.box()
        dimensions = self.screen_data[0].getmaxyx()
        self.pad_position = (position - dimensions[0] + 1 
                + (2 * self.padding))
        self.last_drawn_position = position
        self.local.refresh(self.pad_position, 0, 0, 0, dimensions[0]-1,
                dimensions[1])


class Window(Canvas):
    """A derived class implementing a fixed window.
    """
    def __init__(self, screen_data, rows, border, padding):
        """State constructor.

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

           **rows** (*int*): The number of rows to be rendered by the canvas.

           **border** (*bool*): Should a border be rendered?

           **padding** (*int*): Padding space around the boundaries of the canvas.

        Data Members:
           **self.local** (*curses.window*): Implemented as a curses window with height equal to the rows provided.

           **self.derived** (*curses.window*): Implemented as a curses derived window with the dimensions provided by the screen.
        """
        Canvas.__init__(self, screen_data, rows, border, padding)
        if self.local_rows > self.screen_data[0].getmaxyx()[0]:
            d = ("Error: Unable to initialize HecateWindow with more " 
                    + "rows <" + str(self.local_rows) + "> than are " 
                    + "currently available on the screen <" 
                    + str(self.screen_data[0].getmaxyx()[0]) + ">")
            raise BadInputError(d)
        self.local = curses.newwin(self.local_rows, self.screen_data[1])
        self.derived = self.local.derwin(self.derived_rows, self.max_width, 
                self.padding, self.padding+self.padwad)

    def draw(self):
        """This function renders the relevant portion of the local window to the screen.
        """
        if self.border:
            self.local.box()
        dimensions = self.screen_data[0].getmaxyx()
        self.local.refresh()


class TextInput:
    """A base widget encapsulating an input text box
    """
    def __init__(self, parent, cols, starty, startx, message="", 
            allowed=None):
        """Widget constructor.

        Args:
           **parent** (:class:`Canvas<hecate.widget.Canvas>`): The parent screen widget.

           **cols** (*int*): The column-width of the text input required.

           **starty** (*int*): The y position of the text input in the parent window.

           **startx** (*int*): The x position of the text input in the parent window.

        Kwargs:
           **message** (*str*): The initial text inside the widget. This should be of length less than the window width.

           **allowed** (*str*): A string of characters that are allowable as input for this widget. If None then there are no restrictions.

        Raises:
           DimensionsError, BadInputError

        Data Members:
           **self.window** (*curses.window*): The derived window in which the text input is implemented.

           **self.max_length** (*int*): The width of the window the widget is implemented in.

           **self.active** (*bool*): Flag set to True when the wigdet is in the *entry* function.

        .. versionchanged:: 1.1.1
           
           **parent** argument changed from a *curses.window* to a :class:`Canvas<hecate.widget.Canvas>`-derived object (such as :class:`Pad<hecate.widget.Pad>` or :class:`Window<hecate.widget.Window>`). This is a less flexible design but allows this widget to make draw calls to those parent functions without requiring knowledge of their internal members like scroll positions for the Pad.
        """
        if startx + cols + 1 > parent.canvas().getmaxyx()[1]:
            d = ("Error: Unable to place widget at x-location <"
                    + str(startx) + "> because wiget width <" 
                    + str(cols + 1) + "> will cause it to " 
                    + "be rendered over the edge of the parent "
                    + "window with width <" 
                    + str(parent.canvas().getmaxyx()[1]) + ">.")
            raise DimensionsError(d)
        if starty > parent.canvas().getmaxyx()[0]:
            d = ("Error: Unable to place widget at y-location <"
                    + str(starty) + "> because wiget height <1> will " 
                    + "cause it to be rendered over the edge of the " 
                    + "parent window with width <" 
                    + str(parent.canvas().getmaxyx()[1]) + ">.")
            raise DimensionsError(d)
        self.window = parent.canvas().derwin(1, cols+1, starty, startx)
        self.allowed = allowed
        self.max_length = self.window.getmaxyx()[1] - 1
        if len(message) > self.max_length:
            d = ("Error: Unable to initialize TextInput widget with " 
                    + "message length <" + str(len(message)) + "> greater" 
                    + " than max window length <" + str(self.max_length) 
                    + ">.")
            raise BadInputError(d)
        self.message = message
        self.active = False
        self.parent = parent

    def _build_render(self):
        """This function builds the string to be rendered and pads it appropriately.

        Returns:
           **render** (*str*): The padded string to be used by the *draw* function.
        """
        render = self.message
        while len(render) < self.max_length:
            render += " "
        return render

    def add_character(self, character):
        """This function adds a character to *self.message* if length allows.
        """
        if len(self.message) < self.max_length:
            self.message += character

    def remove_character(self):
        """This function removes a character from *self.message*.
        """
        self.message = self.message[:len(self.message)-1]

    def entry(self):
        """This function accepts keystrokes from the user and returns their completed string.

        Returns:
           **self.message** (*str*): The user generated string.
        """
        self.active = True
        entry = None
        while entry not in [curses.KEY_ENTER, ord("\n")]:
            entry = self.window.getch()
            character = utilities.interpret_text_entry(entry)
            if entry in [curses.KEY_BACKSPACE, 127] or chr(entry) == "\b":
                self.remove_character()
                self.draw()
            elif character:
                if self.allowed:
                    if character in self.allowed:
                        self.add_character(character)
                else:
                    self.add_character(character)
                self.draw()
        self.active = False
        return self.message

    def draw(self):
        """This function draws the widget

        .. versionchanged:: 1.1.1
           The *self.parent.draw()* command at the end of this function allows the parent screen to be rendered after each user keystroke. This was the reason for the change to :class:`Pad.draw()<hecate.widget.Pad.draw>` so that the function would work without requiring a position argument.
        """
        self.window.clear()
        if self.active:
            mode = curses.A_REVERSE
        else:
            mode = curses.A_NORMAL
        self.window.addstr(0, 0, self._build_render(), mode)
        self.parent.draw()

    def activate(self):
        """This function sets the active status to the widget to True and draws it.
        """
        self.active = True
        self.draw()


class SelectableTextInput:
    """A fancier text-input widget implementing a border to indicate selection and a label.
    """
    def __init__(self, parent, cols, starty, startx, label="", 
            orientation=VERTICAL, message="", allowed=None):
        """Widget constructor.

        Args:
            **parent** (:class:`Canvas<hecate.widget.Canvas>`): The parent screen widget.

           **cols** (*int*): The column-width of the text input required.

           **starty** (*int*): The y position of the widget in the parent window.

           **startx** (*int*): The x position of the widget in the parent window.

        Kwargs:
           **label** (*str*): The label to be rendered next to the text input.

           **orientation** (*int*): hecate.widget.VERTICAL or hecate.widget.HORIZONTAL

           **message** (*str*): The initial text inside the widget. This should be of length less than the window width.

           **allowed** (*str*): A string of characters that are allowable as input for this widget. If None then there are no restrictions.

        Raises:
           DimensionsError, BadInputError

        Data Members:
           **self.dimensions** (*list[int, int]*): The dimensions of the widget [width, height].

           **self.max_length** (*int*): The width of the window the widget is implemented in.

           **self.selected** (*bool*): When this value is **True** a border is drawn around the widget.

           **self.text_input** (:class:`TextInput<hecate.widget.TextInput>`): The text entry widget.

        .. versionchanged:: 1.1.1
           
           **parent** argument changed from a *curses.window* to a :class:`Canvas<hecate.widget.Canvas>`-derived object (such as :class:`Pad<hecate.widget.Pad>` or :class:`Window<hecate.widget.Window>`). This was due to the **self.text_input** member of type :class:`TextInput<hecate.widget.TextInput>` which changed in this version.
        """
        self.dimensions = [0, 0]
        self.parent = parent
        self.label = label
        if orientation == VERTICAL:
            self.dimensions[0] = max([cols+1, len(label)+1])
            self.dimensions[1] = 2 + 2
        if orientation == HORIZONTAL:
            self.dimensions[0] = cols + 1 + len(label) + 1
            self.dimensions[1] = 1 + 2
        if starty + 1 > parent.canvas().getmaxyx()[0]:
            d = ("Error: Unable to place widget at y-location <" 
                    + str(starty) + "> because widget height <" 
                    + str(self.dimensions[1]) + "> will cause it to " 
                    + "be rendered over the edge of the parent "
                    + "window with height <" 
                    + str(parent.canvas().getmaxyx()[0]) + ">.")
            raise DimensionsError(d)
        if startx + self.dimensions[0] > parent.canvas().getmaxyx()[1]:
            d = ("Error: Unable to place widget at x-location <" 
                    + str(startx) + "> because widget width <" 
                    + str(self.dimensions[0]) + "> will cause it to " 
                    + "be rendered over the edge of the parent "
                    + "window with width <" 
                    + str(parent.canvas().getmaxyx()[1]) + ">.")
            raise DimensionsError(d)
        if startx < 1:
            d = ("Error: Unable to place SelectableTextInput widget at " 
                    + "x-location less than <1> due to selection border.")
            raise DimensionsError(d)
        if orientation == VERTICAL:
            if starty < 2:
                d = ("Error: Unable to place SelectableTextInput widget " 
                        + "at y-location less than <2> when in VERTICAL " 
                        + "orientation due to selection border.")
                raise DimensionsError(d)
            self.text_input = TextInput(self.parent, cols, starty, startx, 
                    message, allowed)
        elif orientation == HORIZONTAL:
            if starty < 1:
                d = ("Error: Unable to place SelectableTextInput widget " 
                        + "at y-location less than <1> when in HORIZONTAL " 
                        + "orientation due to selection border.")
                raise BadInputError(d)
            self.text_input = TextInput(self.parent, cols, starty, 
                    startx + len(label) + 1, message, allowed)
        else:
            d = ("Error: Unrecognized orientation provided for " 
                    + "SelectableTextInput widget.")
            raise BadInputError(d)
        self.selected = False
        self.orientation = orientation
        self.starty = starty
        self.startx = startx

    def entry(self):
        """This function activates the *self.text_input* member to accept user keystokes.

        Returns:
           (*str*): The return value from the text input entry function. The string typed in by the user.
        """
        self.text_input.activate()
        return self.text_input.entry()

    def select(self):
        """This function causes the border to be drawn around the widget
        """
        self.selected = True

    def deselect(self):
        """This function prevents the border from being drawn around the widget.
        """
        self.selected = False

    def boundaries(self):
        """This function returns a list representing the window co-ordinates of the widget boundaries.

        Returns:
           (*list[int, int, int, int]*): [top_left_y, top_left_x, bottom_right_y, bottom_right_x]
        """
        if self.orientation == VERTICAL:
            return [self.starty-2, self.startx-1, self.starty+1, 
                    self.startx+self.dimensions[0]]
        else:
            return [self.starty-1, self.startx-1, self.starty+1, 
                    self.startx+self.dimensions[0]]

    def draw(self):
        """This function draws the widget in its parent window.
        """
        if self.selected:
            rect = self.boundaries()
            curses.textpad.rectangle(self.parent.canvas(), rect[0], rect[1], 
                    rect[2], rect[3])
        if self.orientation == VERTICAL:
            self.parent.canvas().addstr(self.starty-1, self.startx, self.label)
        else:
            self.parent.canvas().addstr(self.starty, self.startx, self.label)
        self.text_input.draw()

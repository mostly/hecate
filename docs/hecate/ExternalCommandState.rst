ExternalCommand --- A state implementing a call to an external terminal program
===============================================================================

Bases: :class:`hecate.state.BaseState`

.. autoclass:: hecate.state.ExternalCommand
   :members: __init__, set_up, tear_down

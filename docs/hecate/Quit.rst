Quit --- The default Hecate quit state
======================================

Bases: :class:`hecate.state.BaseState`

.. autoclass:: hecate.state.Quit
   :members: __init__

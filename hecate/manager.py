import curses

from . import utilities
from . import state

class ManagerError(Exception):pass
class DuplicateSidError(ManagerError):pass
class ScreenDimensionsError(ManagerError):pass
class BadStateError(ManagerError):pass
class BadSidError(ManagerError):pass


class Manager:
    """A manager class that controls the flow of an application with a state machine.

    Typical program flow involves:

    #. Initializing an object of this class.
    #. Appending states to the object.
    #. Calling the application loop with the starting state

    .. note::

       The curses.window passed to this class during initialization should ideally be generated with **curses.wrapper()**.

    """
    def __init__(self, screen_data, colours=True):
        """Hecate manager constructor

        Args:
           **screen_data** (*_curses.window* or *list*): Curses stdscr and optionally minimum screen width.

        Kwargs:
           **colours** (*bool*): Will terminal colours be used by this application?

        Data Members:
           **self.states** (*dictionary*): The state-dictionary containing all the state objects that may be encountered by the application. The :class:`sid<hecate.state.BaseState.sid>` is used as the dictionary key. The values in this dictionary should be of type :class:`BaseState<hecate.state.BaseState>` or of classes derived from that type.

           **self.current_state** (:class:`BaseState<hecate.state.BaseState>`): The state that is currently active in the application loop.

        .. note::

           During initialization the object will automatically add a :class:`Quit<hecate.state.Quit>` object to its state dictionary. This state can be called to terminate the application.

        .. seealso::

           :class:`load_screen_data<utilities.load_screen_data>`

        """
        self.screen_data = utilities.load_screen_data(screen_data)
        if colours:
            curses.start_color()
            curses.use_default_colors()
            if not curses.has_colors():
                d = ("Error: This application requested the use of " 
                        + "terminal colours and this terminal is unable " 
                        + "to support colours.")
                raise ManagerError(d)
        self.states = {}
        self.append_state(state.Quit(self.screen_data))
        self.current_state = None

    def append_state(self, value):
        """This function appends a state to the class state-dictionary

        Args:
           **value** (:class:`BaseState<hecate.state.BaseState>`) : A :class:`BaseState<hecate.state.BaseState>` object or any object derived from that class to be appended to the state-dictionary.

        Raises:
           DuplicateSidError, ScreenDimensionsError

        .. warning::

           Each state appended to the state dictionary should have a unique sid. During initialization a :class:`Quit<hecate.state.Quit>` object is appended to the class and therefore the sid **hecate-quit** is unusable for all user-added states.
        """
        if value.sid in self.states:
            d = ("Error: Cannot append state with sid already present " 
                    + "in manager <" + value.sid + ">.")
            raise DuplicateSidError(d)
        if self.screen_data[1] < value.screen_data[1]:
            d = ("Error: Unable to add state with max_screen_width <" 
                    + str(value.screen_data[1]) + "> that is larger " 
                    + "than manager screen width <" 
                    + str(self.screen_data[1]) + ">.")
            raise ScreenDimensionsError(d)
        self.states[value.sid] = value

    def application_loop(self, start_state):
        """This function starts the application loop

        Args:
           start_state (str): The sid of the starting state for the application stored in the state-dictionary.

        .. note::

           The flow of the application loop is as follows:

           #. current_state->render()
           #. if current_state->next_state is not **None** and contains an sid present in the state-dictionary, change to that state
           #. current_state->logic()
           #. current_state->get_events()

        .. note::

           During any state change the following flow of events occurs:

           #. current_state.tear_down()
           #. current_state = next_state
           #. next_state = **None**
           #. current_state.set_up()
        """
        self._set_state(start_state)
        while not self.current_state.kill_hecate:
            # Refresh the window
            self.current_state.render()
            # Change state if required
            self._change_state()
            # Perform logic associated with this state
            self.current_state.logic()
            # Handle input if required
            self.current_state.get_events()

    def _change_state(self):
        """Internally used function to change states

        Raises: 
           BadStateError

        .. warning:: 
        
           This function should only be called after :class:`_set_state<hecate.manager.Manager._set_state>` has been called because it makes use of the internal *current_state* variable which is initialized as **None** and only set with that method.
        """
        if not self.current_state:
            d = ("Error: No current state set. Unable to change state " 
                    + "using internal next_state members.")
            raise BadStateError(d)
        if self.current_state.next_state:
            self.current_state.tear_down()
            state_buffer = self.current_state.next_state
            self.current_state.next_state = None
            self._set_state(state_buffer)

    def _set_state(self, sid):
        """Internally used function to set the current state

        Args:
           **sid** (*str*): The sid of a :class:`BaseState<hecate.state.BaseState>` stored in the state-dictionary to be set as the current state.

        Raises:
           BadSidError

        .. note::

           Once the state is set the *set_up* member is automatically called. The *self.from_state* member is set to allow the next state to run conditional code depending on which state it arrived from. This can be used to determine menu position for example.

        .. versionchanged:: 1.2.1
         
           *self.message* member is now passed from the outgoing state to the incoming state. This can be used to transmit data between states.

        """
        if sid not in self.states:
            d = ("Error: Unable to _set_state for sid not present in " 
                    + "manager <" + sid + ">.")
            raise BadSidError(d)
        if self.current_state:
            from_state = self.current_state.sid
            message = self.current_state.message
        else:
            from_state = None
            message = None
        self.current_state = self.states[sid]
        self.current_state.from_state = from_state
        self.current_state.message = message
        self.current_state.set_up()

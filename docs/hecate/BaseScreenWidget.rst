BaseScreen --- The base screen widget class rendered by Hecate
==============================================================

.. autoclass:: hecate.widget.BaseScreen
   :members: __init__

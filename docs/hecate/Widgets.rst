Hecate Widgets Reference
========================
.. toctree::
    :maxdepth: 2

    BaseScreenWidget
    CanvasWidget
    PadWidget
    WindowWidget
    TextInputWidget
    SelectableTextInputWidget

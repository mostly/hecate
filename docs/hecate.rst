API Reference
=============
.. toctree::
    :maxdepth: 2

    hecate/Manager
    hecate/States
    hecate/Widgets
    hecate/Utilities

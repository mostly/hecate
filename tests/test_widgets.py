"""Test hecate widget classes and helper functions"""
import unittest
import curses

from hecate import widget
from hecate import settings
from hecate import utilities

class TestWidgets(unittest.TestCase):
    def setUp(self):
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)

    def tearDown(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    def test_hecatewidget_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.BaseScreen(0)

    def test_hecatewidget_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.BaseScreen([self.stdscr])

    def test_hecatewidget_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.BaseScreen([0, 0])

    def test_hecatewidget_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.BaseScreen([self.stdscr, "hello"])

    def test_hecatewidget_init_window_width_default(self):
        h = widget.BaseScreen(self.stdscr)
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_hecatewidget_init_window_width_custom(self):
        h = widget.BaseScreen([self.stdscr, 60])
        self.assertEquals(h.screen_data[1], 60)

    def test_hecatewidget_init_bad_dimensions(self):
        with self.assertRaises(widget.DimensionsError):
            widget.BaseScreen([self.stdscr, -1])
        with self.assertRaises(widget.DimensionsError):
            widget.BaseScreen([self.stdscr, 6000])

    def test_hecatecanvas_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Canvas(0, 1, False, 0)

    def test_hecatecanvas_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Canvas([self.stdscr], 1, False, 0)

    def test_hecatecanvas_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Canvas([0, 0], 1, False, 0)

    def test_hecatecanvas_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Canvas([self.stdscr, "hello"], 1, False, 0)

    def test_hecatecanvas_init_zero_rows_argument(self):
        with self.assertRaises(widget.BadInputError):
            widget.Canvas(self.stdscr, 0, False, 0)

    def test_hecatecanvas_init_window_width_default(self):
        h = widget.Canvas(self.stdscr, 1, False, 0)
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_hecatecanvas_init_window_width_custom(self):
        h = widget.Canvas([self.stdscr, 60], 1, False, 0)
        self.assertEquals(h.screen_data[1], 60)

    def test_hecatecanvas_init_bad_dimensions(self):
        with self.assertRaises(widget.DimensionsError):
            widget.Canvas([self.stdscr, -1], 1, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.Canvas([self.stdscr, 6000], 1, False, 0)

    def test_hecatecanvas_init_bad_padding(self):
        with self.assertRaises(widget.BadInputError):
            widget.Canvas([self.stdscr, 60], 1, False, -1)
        with self.assertRaises(widget.BadInputError):
            widget.Canvas([self.stdscr, 60], 1, False, 10)

    def test_hecatepad_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Pad(0, 1, False, 0)

    def test_hecatepad_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Pad([self.stdscr], 1, False, 0)

    def test_hecatepad_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Pad([0, 0], 1, False, 0)

    def test_hecatepad_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Pad([self.stdscr, "hello"], 1, False, 0)

    def test_hecatepad_init_window_width_default(self):
        h = widget.Pad(self.stdscr, 1, False, 0)
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_hecatepad_init_window_width_custom(self):
        h = widget.Pad([self.stdscr, 60], 1, False, 0)
        self.assertEquals(h.screen_data[1], 60)

    def test_hecatepad_init_bad_dimensions(self):
        with self.assertRaises(widget.DimensionsError):
            widget.Pad([self.stdscr, -1], 1, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.Pad([self.stdscr, 6000], 1, False, 0)

    def test_hecatepad_init_bad_padding(self):
        with self.assertRaises(widget.BadInputError):
            widget.Pad([self.stdscr, 60], 1, False, -1)
        with self.assertRaises(widget.BadInputError):
            widget.Pad([self.stdscr, 60], 1, False, 10)

    def test_hecatepad_init_zero_rows_argument(self):
        with self.assertRaises(widget.BadInputError):
            widget.Pad(self.stdscr, 0, False, 0)

    def test_hecatewindow_init_bad_single_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Window(0, 1, False, 0)

    def test_hecatewindow_init_short_list_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Window([self.stdscr], 1, False, 0)

    def test_hecatewindow_init_bad_list_screen_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Window([0, 0], 1, False, 0)

    def test_hecatewindow_init_bad_list_screen_width_argument(self):
        with self.assertRaises(utilities.BadScreenDataError):
            widget.Window([self.stdscr, "hello"], 1, False, 0)

    def test_hecatewindow_init_window_width_default(self):
        h = widget.Window(self.stdscr, 1, False, 0)
        self.assertEquals(h.screen_data[1], settings.MIN_SCREEN_WIDTH)

    def test_hecatewindow_init_window_width_custom(self):
        h = widget.Window([self.stdscr, 60], 1, False, 0)
        self.assertEquals(h.screen_data[1], 60)

    def test_hecatewindow_init_bad_dimensions(self):
        with self.assertRaises(widget.DimensionsError):
            widget.Window([self.stdscr, -1], 1, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.Window([self.stdscr, 6000], 1, False, 0)

    def test_hecatewindow_init_bad_padding(self):
        with self.assertRaises(widget.BadInputError):
            widget.Window([self.stdscr, 60], 1, False, -1)
        with self.assertRaises(widget.BadInputError):
            widget.Window([self.stdscr, 60], 1, False, 10)

    def test_hecatewindow_init_zero_rows_argument(self):
        with self.assertRaises(widget.BadInputError):
            widget.Window(self.stdscr, 0, False, 0)

    def test_hecatewindow_init_too_many_rows(self):
        with self.assertRaises(widget.BadInputError):
            widget.Window([self.stdscr, 60], 6000, False, 0)

    def test_textinput_init_too_wide_for_parent(self):
        w = widget.Window([self.stdscr, 10], 10, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.TextInput(w, 10, 0, 0, message="12345678910")

    def test_textinput_init_long_message(self):
        w = widget.Window([self.stdscr, 10], 10, False, 0)
        with self.assertRaises(widget.BadInputError):
            widget.TextInput(w, 9, 0, 0, message="12345678910")

    def test_textinput_init_max_length(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0)
        self.assertEquals(t.max_length, 9)

    def test_textinput_build_render(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0, message="")
        t.message = "12345"
        self.assertEquals(t._build_render(), "12345    ")

    def test_textinput_add_character(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0, message="12345678")
        self.assertEquals(t.message, "12345678")
        t.add_character("9")
        self.assertEquals(t.message, "123456789")
        t.add_character("X")
        self.assertEquals(t.message, "123456789")

    def test_textinput_remove_character(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0, message="12")
        self.assertEquals(t.message, "12")
        t.remove_character()
        self.assertEquals(t.message, "1")
        t.remove_character()
        self.assertEquals(t.message, "")
        t.remove_character()
        self.assertEquals(t.message, "")

    def test_textinput_draw_from_window(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0)
        t.draw()

    def test_textinput_draw_from_pad(self):
        w = widget.Pad([self.stdscr, 10], 5, False, 0)
        t = widget.TextInput(w, 9, 0, 0)
        t.draw()

    def test_selectabletextinput_init(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t = widget.SelectableTextInput(w, 5, 2, 1, "t")
        self.assertEquals(t.dimensions, [6, 4])

    def test_selectabletextinput_init_long_label_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 5, 2, 1, "123456789")

    def test_selectabletextinput_init_long_cols_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 2, 1)

    def test_selectabletextinput_init_large_starty_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 4, 1)

    def test_selectabletextinput_init_small_starty_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 1, 1)

    def test_selectabletextinput_init_small_startx_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 4, 0)

    def test_selectabletextinput_init_long_label_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 5, 2, 1, "123", 
                    widget.HORIZONTAL)

    def test_selectabletextinput_init_long_cols_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 8, 2, 1, 
                    orientation=widget.HORIZONTAL)

    def test_selectabletextinput_init_large_starty_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 4, 1, 
                    orientation=widget.HORIZONTAL)

    def test_selectabletextinput_init_small_starty_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 0, 1,
                    orientation=widget.HORIZONTAL)

    def test_selectabletextinput_init_small_startx_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        with self.assertRaises(widget.DimensionsError):
            widget.SelectableTextInput(w, 9, 4, 0,
                    orientation=widget.HORIZONTAL)

    def test_selectabletextinput_boundaries_vertical(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t =  widget.SelectableTextInput(w, 5, 2, 1, "t",
                    orientation=widget.VERTICAL)
        self.assertEquals([0, 0, 3, 7], t.boundaries())

    def test_selectabletextinput_boundaries_horizontal(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t =  widget.SelectableTextInput(w, 5, 2, 1, "t",
                    orientation=widget.HORIZONTAL)
        self.assertEquals([1, 0, 3, 9], t.boundaries())

    def test_selectabletextinput_draw_from_window(self):
        w = widget.Window([self.stdscr, 10], 5, False, 0)
        t =  widget.SelectableTextInput(w, 5, 2, 1, "t",
                    orientation=widget.VERTICAL)
        t.draw()

    def test_selectabletextinput_draw_from_pad(self):
        w = widget.Pad([self.stdscr, 10], 5, False, 0)
        t =  widget.SelectableTextInput(w, 5, 2, 1, "t",
                    orientation=widget.VERTICAL)
        t.draw()


# import the required modules
import curses
import hecate


class QuitState(hecate.state.BaseState):
    def __init__(self, screen, sid):
        hecate.state.BaseState.__init__(self, screen, sid)
        self.events_allowed = True
        self.window = hecate.widget.Window(self.screen_data, 5, True, 1)

    def set_up(self):
        self.window.local.clear()

    def logic(self):
        self.window.canvas().addstr(0, 0, "Quit state")
        self.window.canvas().addstr(1, 0, "Hit any key to continue")
        self.window.draw()

    def handle_events(self, event):
        if event:
            self.next_state = "example-root"


class SaveState(hecate.state.BaseState):
    def __init__(self, screen, sid):
        hecate.state.BaseState.__init__(self, screen, sid)
        self.events_allowed = True
        self.window = hecate.widget.Window(self.screen_data, 10, True, 1)

    def set_up(self):
        self.window.local.clear()

    def logic(self):
        self.window.canvas().addstr(0, 0, "Save state")
        form_list = []
        for element in self.message:
            for item in element:
                form_list.append(item)
        self.window.canvas().addstr(1, 0,
                "first-name : {}".format(form_list[0]))
        self.window.canvas().addstr(2, 0,
                "surname : {}".format(form_list[1]))
        self.window.canvas().addstr(3, 0,
                "age : {}".format(form_list[2]))
        self.window.canvas().addstr(4, 0,
                "address : {}".format(form_list[3]))
        self.window.canvas().addstr(5, 0,
                "species : {}".format(form_list[4]))
        if "" in form_list:
            self.window.canvas().addstr(6, 0, 
                    "Empty fields detected, not saving.")
        self.window.canvas().addstr(8, 0, "Hit any key to continue")
        self.window.draw()
        self.message = None

    def handle_events(self, event):
        if event:
            self.next_state = "example-root"


# the wrapped main loop
def entry(stdscr):
    # the manager
    h = hecate.Manager(stdscr)
    # the root menu
    state_id = "example-root"
    menu_heading = "Example Menu"
    menu_sub_heading = "Input form examples"
    options = ["Vertical Form Input Example", 
            "Horizontal Form Input Example","Exit"]
    states = ["example-01", "example-02", "hecate-quit"]
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        state_id, menu_heading, menu_sub_heading, options, states)
    )
    # example-01
    h.append_state(hecate.state.InputForm(h.screen_data, "example-01",
        "Vertical Form", "<SPACE> to save | <ESCAPE> to quit", 
        [20, 20, 3, 50, 15], 
        ["First Name", "Surname", "Age", "Address", "Species"],
        ["", "", "", "", "Zebra"],
        [None, None, "0123456789", None, None],
        hecate.widget.VERTICAL, "example-quit", "example-save"))
    # example-02
    h.append_state(hecate.state.InputForm(h.screen_data, "example-02",
        "Horizontal Form", "<SPACE> to save | <ESCAPE> to quit", 
        [20, 20, 3, 50, 15], 
        ["First Name", "Surname", "Age", "Address", "Species"],
        ["", "", "", "", "Zebra"],
        [None, None, "0123456789", None, None],
        hecate.widget.HORIZONTAL, "example-quit", "example-save"))
    # quit state
    h.append_state(QuitState(h.screen_data, "example-quit"))
    # save state
    h.append_state(SaveState(h.screen_data, "example-save"))
    # start the application loop
    h.application_loop("example-root")

# cleanly initializing curses
if __name__=="__main__":
    curses.wrapper(entry)

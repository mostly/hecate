# hecate

![hecate](docs/logo.png)

## What is it?
Hecate is a python module that implements an *ncurses-based* state machine. Its original intented use was for menus but you can implement other functionality by deriving from the base-class state.

![Are these the drones you are looking for?](docs/screenshot.png)

## Why?
This is a python module that was primarily born out of a need to implement **simple** command-line menus that go on to execute other curses-based functions. I was unable to find an existing solution that didn't keep threads running that prevented further curses based functionality from being executed easily. Hecate requires nothing outside of the python core libraries and was designed with *ease-of-use* as the major design goal.

## How is it used?
Hecate is implemented as a state-machine manager which executes an application loop that calls a predefined set of object methods. It is a framework that provides the user flexibility in how they want to implement their own tasks. This may sound really complex but lets build a simple menu to see how it is done.

**Note 1:** *All of this code can be found in the examples folder of the package.*

**Note 2:** *If you wish to used the package after reviewing this example the installation is described in the next section.*

    # import the required modules
    import curses
    import hecate

    # the wrapped main loop
    def entry(stdscr):
        # the manager
        h = hecate.Manager(stdscr)
        # the root menu
        state_id = "example-root"
        menu_heading = "Example Menu"
        menu_sub_heading = "Si non confectus non reficiat"
        options = ["Submenu", "Exit"]
        states = ["example-submenu", "hecate-quit"]
        h.append_state(hecate.state.StateShiftMenu(h.screen_data,
            state_id, menu_heading, menu_sub_heading, options, states)
        )
        # the sub-menu
        h.append_state(hecate.state.StateShiftMenu(h.screen_data,
            "example-submenu", "Easy Mode", "idpopqd", 
            ["Custom Command", "Return to Main"],
            ["example-menu", "example-menu"], border=False, 
            padding=4)
        ) 
        # start the application loop
        h.application_loop("example-root")

    # cleanly initializing curses
    if __name__=="__main__":
        curses.wrapper(entry)

Thats all thats needed to build, an admittedly simple, menu traversing application. The example shows some of the aesthetic options that may be employed using one of the default state-classes (StateShiftMenu), but menus should eventually lead to something. Lets create our own custom state to highlight the program flow of methods in the state machine.

Your custom state should be derived from hecate's **BaseState**, and from there you can override any of the default functions that are called by the application loop or you can add your own functions. The default flow of the application loop is as follows:
1. *self.set\_up()* - when a state becomes the **current\_state**.
2. *self.render()* - default **BaseState** behavior clears the screen.
3. *manager.change\_state* - the manager changes the **current\_state** if it wants to change to something else.
4. *self.logic()* - default **BaseState** behavior does nothing here.
5. *self.get\_events()* - default **BaseState** behaviour does nothing here.

Your custom state can also 3 useful data members:
1. *self.events\_allowed* - causes default **BaseState**.*get\_events()* get a character from the user and call **BaseState**.*handle\_events()*.
2. *self.sid* - the unique string identifiying this state to the manager when it is appended.
3. *self.next\_state* - if this contains the *sid* of a state appended to the manager then during the next manager application loop that will become the current state. The default value is **None**.

Lets create a custom state that prints each of its methods that have been called and then returns to the sub-menu when the user presses a key:

    # custom state
    class MyState(hecate.state.BaseState):
        def __init__(self, screen, sid):
            hecate.state.BaseState.__init__(self, screen, sid)
            # We want to get the user keypresses
            self.events_allowed = True
            # The call list that will be printed
            self.call_list = []
            # For simpler curses calls
            self.screen = self.screen_data[0]

        def set_up(self):
            self.call_list.append("set_up")

        def render(self):
            self.call_list.append("render")
            # A curses clear screen
            self.screen.clear()
            self.screen.refresh()
            curses.doupdate()

        def logic(self):
            self.call_list.append("logic")
            self.screen.addstr(0, 0, str(self.call_list))
            self.call_list = []

        def handle_events(self, event):
            self.call_list.append("handle_events")
            if event in [curses.KEY_ENTER, ord("\n")]:
                self.next_state = "example-submenu"
            else:
                curses.flash()
                curses.beep()

Now in the *entry()* function of our program we must append an object of this state to the manager and update the sub-menu to call this state when the *"Custom Command"* option is selected from the submenu:

    # the sub-menu
    h.append_state(hecate.state.StateShiftMenu(h.screen_data,
        "example-submenu", "Easy Mode", "idpopqd",
        ["Custom Command", "Return to Main"],
        ["mystate", "example-root"], border=False,
        padding=4)
    )
    # the custom state
    h.append_state(MyState(h.screen_data, "mystate"))

This full example can be found [here](https://gitlab.com/mostly/hecate/blob/master/examples/01_custom_state.py).

## How do I install this to use it?
### Acquiring
Hecate can currently only be obtained through gitlab:
1. For git users:
    git clone https://gitlab.com/mostly/hecate.git
2. For non-git users, download a zip and extract it.

### Installing
The top level folder containing **this** README file will be referred to as the *root folder*.

Hecate only makes use of python core modules so it should not require installation of additional packages. This package was built and tested on Python 3.6.4 but should be compatible with Python 3.4.x onwards.

Hecate is deployed using pip. Change to the *root folder* and type:
1. For system-wide installations:
    pip install --upgrade .
2. For local-user installations:
    pip install --user --upgrade .

## Additional information sources
### Project documentation
This package uses **Sphinx** to auto-generate documentation. The package docs can be built by changing to the **docs** folder in the *root folder* and typing:
    sphinx-build -b html ./ _build/

The documentation can then be browsed at: *docs/_build/index.html*

### Examples
* [Custom state example](https://gitlab.com/mostly/hecate/blob/master/examples/01_custom_state.py)
* [The from\_state field](https://gitlab.com/mostly/hecate/blob/master/examples/02_from_state_field.py)
* [Text input widgets](https://gitlab.com/mostly/hecate/blob/master/examples/03_text_input_widgets.py)
* [Input forms](https://gitlab.com/mostly/hecate/blob/master/examples/04_input_form.py)

## Hecate
As for the project name, Hecate; daughter of the Titans Perses and Asteria, the goddess of Greek mythology whose purview was magic, sorcery, witchcraft, crossroads and trivial knowledge. Mentioned along with the troublesome witches of Shakespeare's Macbeth she represents my *ncurses*-caster if you will ;)

